// make nav items work
$(".nav-item").click(function() {
  let section = $(this).data("section");

  $("section, .nav-item").removeClass("active");
  $(this).addClass("active");
  $("section." + section).addClass("active");

  if (section == "map" && mapLoaded == false) {
    showMap()
    networkMap.fitBounds(stationFeatureGroup.getBounds())
    mapLoaded = true;
  }
})

let mapLoaded = false;

// get network info
let params = new URLSearchParams(location.search);
let network = params.get("network")
let api = "pk.eyJ1IjoiZHV4a2doIiwiYSI6ImNrYnJ4NXJwNzMwbDYyeWw5eG1hcWxpZ2cifQ.oaq3znG3kyF3JC1YlXtamA"
let lat;
let lon;
let stations = [];

$.get("https://api.citybik.es/v2/networks/" + network, function(results) {
  lat = results.network.location.latitude;
  lon = results.network.location.longitude;
  stations = results.network.stations
})
  .done(function() {
    showStations();

  })
  .fail(function() {
    alert("Couldn't load networks. Please reload the page.");
  })

var networkMap = L.map("map", {
  touchZoom: true,
})

function showMap() {
  L.tileLayer("https://api.mapbox.com/styles/v1/duxkgh/ckbtd5iq40s1u1iqsbr08e7vd/tiles/512/{z}/{x}/{y}@2x?access_token=" + api, {
    maxZoom: 18,
    attribution: '<a href="https://www.openstreetmap.org/">OpenStreetMap</a> | ' + '<a href="https://www.mapbox.com/">Mapbox</a>',
    id: "mapbox/streets-v11",
    tileSize: 512,
    zoomOffset: -1,
  }).addTo(networkMap);

  networkMap.setView([lat, lon], 15);

  // create control and add to map
  lc = L.control.locate({
    showPopup: false,
    icon: "gps-location",
    iconLoading: "gps-loading",
    setView: false,
    clickBehavior: {
      inView: "setView",
      outOfView: "setView",
      inViewNotFollowing: "setView"
    },
    locateOptions: {
      enableHighAccuracy: true
    }}).addTo(networkMap);

  lc.start();
};

let lc;

function stationColour(b) {
  return b > 5 ? "#55c10b" :
         b > 2 ? "#df973a" :
         b > 0 ? "#d22020" :
                 "#000";
}

function showStations() {
  let stationFeatures = []

  $(stations).each(function() {
    // remove loading spinner
    $(".loading-tr").remove();

    // in list
    $("table tbody.full-list").append("<tr data-station='" + this.id + "'><td class='network-name'>" + this.name + "</td><td class='bikes'>" + this.free_bikes + "</td><td class='slots'>" + this.empty_slots + "</td></tr>")

    // enable search in list
    $("#search").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("tbody.full-list tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });

    // add to select
    $("#from, #to").append("<option value=" + this.id + ">" + this.name + "</option>")

    // on map
    let circleColor = stationColour(this.free_bikes);

    let stationCircle = L.circleMarker([this.latitude, this.longitude], {
      color: circleColor,
      fillColor: circleColor,
      fillOpacity: .5,
      properties: {
        station: this.id
      }
    });

    stationFeatures.push(stationCircle);
  });

  stationFeatureGroup = L.featureGroup(stationFeatures).on("click", openStationFromMap);

  stationFeatureGroup.addTo(networkMap)

  // click on station to open it in the list
  $("tr[data-station]").click(function() {
    let station = $(this).data("station");
    openStation(station)
  })

  // click on a station to open it from the map
  function openStationFromMap(e) {
    openStation(e.layer.options.properties.station)
  }
};

let stationFeatureGroup;
let openedStation;

// change circle radius based on zoom level
networkMap.on("zoomend", function() {
  let zoom = networkMap.getZoom();
  let difference = 19 - zoom;

  let radius = zoom - difference;

  stationFeatureGroup.setStyle({
    radius: radius
  })
});

// open single station
function openStation(station) {
  let selectedStation = stations.find(item => item.id === station);

  $("#station-details .name").text(selectedStation.name)
  $("#station-details .address").text(selectedStation.extra.address)
  $("#station-details .bikes").text(selectedStation.free_bikes)
  $("#station-details .slots").text(selectedStation.empty_slots)

  $("#station-details").addClass("active")

  openedStation = station;
}

// close station
function closeStation() {
  $("#station-details").removeClass("active");
  $("body").removeClass("station-open");
}

// open directions
function openDirections() {
  $("#directions").addClass("active")
  $("body").addClass("directions-open");
}

// close directions
function closeDirections() {
  $("#directions").removeClass("active")
  $("body").removeClass("directions-open");
}

function openRoute() {
  $("#route").addClass("active")
  $("body").addClass("route-open");
  $("#start-route img").attr("src", "img/bicycle.svg").removeClass("loading");
}

function closeRoute() {
  $("#route").removeClass("active")
  $("body").removeClass("route-open");
}

// set departure through button
$("#leave-from-here").click(function() {
  $("#from").val(openedStation);
  closeStation()
  openDirections()
})

// set arrive through button
$("#go-here").click(function() {
  $("#to").val(openedStation);
  closeStation()
  openDirections()
})

// click button to close station
$("#close-station").click(function() {
  closeStation();
})

let mapLines = [];

// get route
function getRoute() {
  $("#start-route img").attr("src", "img/loading.svg").addClass("loading");

  let departureID = $("#from").val()
  let arrivalID = $("#to").val()

  let arrivalStation = stations.find(item => item.id === arrivalID);
  let departureStation;

  // get gps location
  if (departureID == "gps") {
    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };

    function success(pos) {
      var crd = pos.coords;

      departureStation = {
        "latitude": crd.latitude,
        "longitude": crd.longitude
      }

      ajaxCall(departureStation, arrivalStation)
    }

    function error(err) {
      window.alert("Unable to get location")
    }

    navigator.geolocation.getCurrentPosition(success, error, options);

  } else {
    departureStation = stations.find(item => item.id === departureID);
    ajaxCall(departureStation, arrivalStation)
  }

}

// ajax call to get data
function ajaxCall(departureStation, arrivalStation) {
  let x1 = departureStation.latitude
  let y1 = departureStation.longitude
  let x2 = arrivalStation.latitude
  let y2 = arrivalStation.longitude

  $.get("https://api.mapbox.com/directions/v5/mapbox/cycling/" + y1 + "," + x1 + ";" + y2 + "," + x2 + "?alternatives=false&geometries=geojson&steps=false&access_token=pk.eyJ1IjoiZHV4a2doIiwiYSI6Ijg5NWE2ZWI5NmVhNTVmMGIyMjhkNDJhYmViNmI1ZjNjIn0.u2_1ZH77JRRo2JzCkcjbrw")
  .done(function(results) {
    let routeLines = [{
      "type": "LineString",
      "coordinates": results.routes[0].geometry.coordinates,
    }];

    networkMap.removeLayer(mapLines);

    mapLines = L.geoJSON(routeLines, {
      style: {
        "color": "#1306d1",
        "weight": 2,
        "opacity": 1
      }
    })

    let distance = (results.routes[0].distance / 1000).toPrecision(2)
    let duration = Math.round(results.routes[0].duration / 60)

    $("#distance").text(distance + "km, ")
    $("#duration").text("about " + duration + " minutes")

    mapLines.addTo(networkMap);
    networkMap.fitBounds(mapLines.getBounds());

    closeStation()
    closeDirections()
    openRoute()
  })
  .fail(function() {
    alert("Couldn't get route. Please reload the page.");
  })
}

// click on button to get route
$("#start-route").click(function() {
  getRoute()
  $(".nav-item[data-section='map']").click();
})

// click on button to cancel route
$("#cancel-route").click(function() {
  networkMap.removeLayer(mapLines);
  closeRoute();
})

//